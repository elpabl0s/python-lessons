import os
import time

source = ['/home/test1','/home/test2']

target_dir = '/home/backup/'

target = target_dir + time.strftime('%Y%m%m%H%M%S') + '.zip'

zip_command = "zip -qr '%s' %s" % (target, ' '.join(source))

if os.system(zip_command) == 0:
    print ('Successful backup to', target)
else:
    print ('Backup FAILED')
