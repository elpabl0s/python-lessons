x = int(input())
y = int(input())

def result(x, y, name='per'):
    def per(x,y):
        return (2*(x+y))
    def sqr(x,y):
        return (x*y)
    if name == 'per':
        return per(x,y)
    elif name == 'sqr':
        return sqr(x,y)

s = result (x,y, 'sqr')
p = result(x,y, 'per')

print(s,p)


