x = int(input())
y = int(input())

def fun (fx, fy):
        s = lambda fx, fy: fx*fy
        p = lambda fx, fy: 2*(fx+fy)
        return [str(s(fx,fy)), str(p(fx, fy))]
print(' '.join(fun(x, y)))
